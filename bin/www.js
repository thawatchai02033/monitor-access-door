#!/usr/bin/env node

/**
 * Module dependencies.
 */

var app = require('../app');
var debug = require('debug')('monitor-access-door2:server');
var http = require('http');

var mysql = require("mysql");
var axios = require('axios');
var querystring = require('querystring');

/**
 * Get port from environment and store in Express.
 */
var host = '172.29.1.6'
var user = 'HRTIME'
var password = '*HRTIME*'
var database = 'HRTIME_DB'

/* HRTIME Host */
var pool = mysql.createPool({
    connectionLimit: 0,
    host: host,
    user: user,
    password: password,
    database: database,
    waitForConnections: true,
    debug: false
});

var Cookies = ''
var checkLength = 0

var port = normalizePort(process.env.PORT || '3012');
app.set('port', port);

/**
 * Create HTTP server.
 */

var server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

//monitor_device
setInterval(() => {
    checkLength = 0
    pool = mysql.createPool({
        connectionLimit: 0,
        host: host,
        user: user,
        password: password,
        database: database,
        waitForConnections: true,
        debug: false
    })
    getDataFormZK().then(res => {
        console.log('ดึงข้อมูลเวลา -> ' + getTime())
        if (res.data.status) {
            if (res.data.data != null && res.data.data.length > 0) {
                res.data.data.map(Device_i => {
                    getDataFormDB(`SELECT *
                                   FROM monitor_device
                                   WHERE id in ( SELECT MAX(id) FROM HRTIME_DB.monitor_device WHERE Serial_Number = '${Device_i.data[1]}')`).then(function (resData) {
                        checkLength++
                        if (checkLength == res.data.data.length) {
                            pool.end()
                        }
                        if (resData.data.status) {
                            if (resData.data.data.length > 0) {
                                if (resData.data.data[0].IS_Exception != Device_i.userdata.status) {
                                    console.log(resData.data.data[0].IS_Exception + ' - ' + Device_i.userdata.status)
                                    const headerAxios = {
                                        headers: {
                                            'Content-Type': 'application/json',
                                            'X-REST-METHOD': 'PUT'
                                        }
                                    }
                                    axios.post('https://medhr.medicine.psu.ac.th/app-api/v2/?/apis/hrtime/monitor_device/', {
                                        Device_Name: Device_i.data[0],
                                        Serial_Number: Device_i.data[1],
                                        Area: Device_i.data[2],
                                        Operation_Status: Device_i.data[3],
                                        Current_Status: Device_i.data[4],
                                        Commands_List: Device_i.data[5],
                                        Recently_Abnormal_State: Device_i.data[6],
                                        Operations: Device_i.data[7],
                                        Area_Id: Device_i.userdata.areaId,
                                        IS_Exception: Device_i.userdata.status,
                                    }, headerAxios).then(function (res) {
                                        if (res.data.status) {
                                            if (Device_i.userdata.status == 'exception') {
                                                SendLine('อุปกรณ์ขัดข้อง !! \n ชื่ออุปกรณ์ - ' + Device_i.data[0] + ' พื้นที่ - ' + Device_i.data[2] + ' สถานะ ' + Device_i.data[4] + ' (' + Device_i.data[3] + ')' + ' วัน:เวลา - ' + getTime())
                                                console.log('อุปกรณ์ขัดข้อง !! \n ชื่ออุปกรณ์ - ' + Device_i.data[0] + ' พื้นที่ - ' + Device_i.data[2] + ' สถานะ ' + Device_i.data[4] + ' (' + Device_i.data[3] + ')' + ' วัน:เวลา - ' + getTime())
                                            } else {
                                                axios.get('https://medhr.medicine.psu.ac.th/app-api/v2/?/apis/hrtime/monitor_device/Serial_Number/' + resData.data.data[0].Serial_Number + '/?sort=id_Desc&limit=1').then(resGetData => {
                                                    if (resGetData.data) {
                                                        var date = new Date(resData.data.data[0].create_time);
                                                        pool = mysql.createPool({
                                                            connectionLimit: 0,
                                                            host: host,
                                                            user: user,
                                                            password: password,
                                                            database: database,
                                                            waitForConnections: true,
                                                            debug: false
                                                        })
                                                        getDataFormDB('' +
                                                            'SELECT TIMESTAMPDIFF(MINUTE,\'' + (date.toISOString().split('T')[0] + ' ' + date.toTimeString().split(' ')[0]) + '\',\'' + resGetData.data.create_time + '\') AS TIMEDATA; ' +
                                                            '').then(function (resData) {
                                                                pool.end()
                                                            if (resData.data.status && resData.data.data.length > 0) {
                                                                SendLine('อุปกรณ์กลับมาทำงานปกติ  !! \n ชื่ออุปกรณ์ - ' + Device_i.data[0] + ' พื้นที่ - ' + Device_i.data[2] + ' กลับมาทำงานปกติเมื่อ วัน:เวลา - ' + resGetData.data.create_time + ' ใช้ระยะเวลาโดยประมาณ - ' + timeConvert(resData.data.data[0].TIMEDATA))
                                                                console.log('อุปกรณ์กลับมาทำงานปกติ  !! \n ชื่ออุปกรณ์ - ' + Device_i.data[0] + ' พื้นที่ - ' + Device_i.data[2] + ' กลับมาทำงานปกติเมื่อ วัน:เวลา - ' + resGetData.data.create_time + ' ใช้ระยะเวลาโดยประมาณ - ' + timeConvert(resData.data.data[0].TIMEDATA))
                                                            }
                                                        })
                                                    }
                                                })
                                            }
                                        }
                                    })
                                } else {
                                    if (Device_i.userdata.status == 'exception' && resData.data.data[0].Operation_Status != Device_i.data[3] && resData.data.data[0].Current_Status != Device_i.data[4]) {
                                        const headerAxios = {
                                            headers: {
                                                'Content-Type': 'application/json',
                                                'X-REST-METHOD': 'PUT'
                                            }
                                        }
                                        axios.post('https://medhr.medicine.psu.ac.th/app-api/v2/?/apis/hrtime/monitor_device/', {
                                            Device_Name: Device_i.data[0],
                                            Serial_Number: Device_i.data[1],
                                            Area: Device_i.data[2],
                                            Operation_Status: Device_i.data[3],
                                            Current_Status: Device_i.data[4],
                                            Commands_List: Device_i.data[5],
                                            Recently_Abnormal_State: Device_i.data[6],
                                            Operations: Device_i.data[7],
                                            Area_Id: Device_i.userdata.areaId,
                                            IS_Exception: Device_i.userdata.status,
                                        }, headerAxios).then(function (res) {
                                            if (res.data.status) {
                                                    SendLine('อุปกรณ์ขัดข้อง !! \n ชื่ออุปกรณ์ - ' + Device_i.data[0] + ' พื้นที่ - ' + Device_i.data[2] + ' สถานะ ' + Device_i.data[4] + ' (' + Device_i.data[3] + ')' + ' วัน:เวลา - ' + getTime())
                                                    console.log('อุปกรณ์ขัดข้อง !! \n ชื่ออุปกรณ์ - ' + Device_i.data[0] + ' พื้นที่ - ' + Device_i.data[2] + ' สถานะ ' + Device_i.data[4] + ' (' + Device_i.data[3] + ')' + ' วัน:เวลา - ' + getTime())
                                            }
                                        })
                                    }
                                }
                            } else {
                                // if (Device_i.userdata.status == 'exception') {
                                const headerAxios = {
                                    headers: {
                                        'Content-Type': 'application/json',
                                        'X-REST-METHOD': 'PUT'
                                    }
                                }
                                axios.post('https://medhr.medicine.psu.ac.th/app-api/v2/?/apis/hrtime/monitor_device/', {
                                    Device_Name: Device_i.data[0],
                                    Serial_Number: Device_i.data[1],
                                    Area: Device_i.data[2],
                                    Operation_Status: Device_i.data[3],
                                    Current_Status: Device_i.data[4],
                                    Commands_List: Device_i.data[5],
                                    Recently_Abnormal_State: Device_i.data[6],
                                    Operations: Device_i.data[7],
                                    Area_Id: Device_i.userdata.areaId,
                                    IS_Exception: Device_i.userdata.status,
                                }, headerAxios).then(function (res) {
                                    if (res.data.status) {
                                        if (Device_i.userdata.status == 'exception') {
                                            SendLine('อุปกรณ์ขัดข้อง !! \n ชื่ออุปกรณ์ - ' + Device_i.data[0] + ' พื้นที่ - ' + Device_i.data[2] + ' สถานะ ' + Device_i.data[4] + ' (' + Device_i.data[3] + ')' + ' วัน:เวลา - ' + getTime())
                                            console.log('อุปกรณ์ขัดข้อง !! \n ชื่ออุปกรณ์ - ' + Device_i.data[0] + ' พื้นที่ - ' + Device_i.data[2] + ' สถานะ ' + Device_i.data[4] + ' (' + Device_i.data[3] + ')' + ' วัน:เวลา - ' + getTime())
                                        }
                                    } else {

                                    }
                                })
                                // }
                            }
                        }
                    })
                })
            }
        }
    })
}, 8000)


/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
    var port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    var bind = typeof port === 'string'
        ? 'Pipe ' + port
        : 'Port ' + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
    var addr = server.address();
    var bind = typeof addr === 'string'
        ? 'pipe ' + addr
        : 'port ' + addr.port;
    debug('Listening on ' + bind);
}

function timeConvert(n) {
    var num = n;
    var hours = (num / 60);
    var rhours = Math.floor(hours);
    var minutes = (hours - rhours) * 60;
    var rminutes = Math.round(minutes);
    return rhours + " ช.ม. " + rminutes + " นาที";
}

function getDataFormDB(query) {
    return new Promise(function (resolutionFunc, rejectionFunc) {
        // pool = mysql.createPool({
        //     connectionLimit: 0,
        //     host: host,
        //     user: user,
        //     password: password,
        //     database: database,
        //     waitForConnections: true,
        //     debug: false
        // })
        pool.getConnection(function (err, connection) {
            try {
                if (connection) {
                    connection.query(query, function (err, rows) {
                        // pool.end()
                        if (!err) {
                            resolutionFunc({data: {status: true, data: rows}})
                        } else {
                            resolutionFunc({data: {status: false, data: err}})
                        }
                    });
                }
            } catch (e) {
                console.log(e)
                console.log('error time => ' + new Date().toLocaleString())
            } finally {
                connection.release();
            }
        })
    });
}


function getTime() {
    var currentTime = new Date();

    var currentHours = currentTime.getHours();
    var currentMinutes = currentTime.getMinutes();
    var currentSeconds = currentTime.getSeconds();

    // Pad the minutes and seconds with leading zeros, if required
    currentMinutes = (currentMinutes < 10 ? "0" : "") + currentMinutes;
    currentSeconds = (currentSeconds < 10 ? "0" : "") + currentSeconds;

    // Choose either "AM" or "PM" as appropriate
    var timeOfDay = (currentHours < 12) ? "AM" : "PM";

    // Convert the hours component to 12-hour format if needed
    currentHours = (currentHours > 12) ? currentHours - 12 : currentHours;

    // Convert an hours component of "0" to "12"
    currentHours = (currentHours == 0) ? 12 : currentHours;

    // Compose the string for display
    var currentTimeString = currentHours + ":" + currentMinutes + ":" + currentSeconds + " " + timeOfDay;

    // Update the time display
    return (getDateNow() + ' - ' + currentTimeString)
}

var getDateNow = function () {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    var hour = today.getHours();
    var minute = today.getMinutes();
    var second = today.getSeconds();
    if (hour.toString().length == 1) {
        hour = '0' + hour;
    }
    if (minute.toString().length == 1) {
        minute = '0' + minute;
    }
    if (second.toString().length == 1) {
        second = '0' + second;
    }
    return yyyy + '-' + mm + '-' + dd
}

function getDeviceData() {
    return new Promise(resolve => {
        axios.get('https://medhr.medicine.psu.ac.th/app-api/v2/?/apis/hrtime/monitor_device/').then(res => {
            if (res.data) {
                if (res.data.length > 0) {
                    resolve({
                        data: {
                            status: true,
                            data: res.data
                        }
                    })
                } else {
                    resolve({
                        data: {
                            status: true,
                            data: [res.data]
                        }
                    })
                }

            } else {
                resolve({
                    data: {
                        status: true,
                        data: null
                    }
                })
            }
        })
    })
}

function CheckData(item) {
    return new Promise((resolve) => {
        axios.get('https://medhr.medicine.psu.ac.th/app-api/v2/?/apis/hrtime/monitor_device/Serial_Number/' + item.id + '/?sort=id_Desc&limit=1/').then(function (res) {
            if (res.data) {
                if (res.data.IS_Exception != 'exception') {
                    const headerAxios = {
                        headers: {
                            'Content-Type': 'application/json',
                            'X-REST-METHOD': 'PUT'
                        }
                    }
                    axios.post('https://medhr.medicine.psu.ac.th/app-api/v2/?/apis/hrtime/monitor_device/', {
                        Device_Name: item.data[0],
                        Serial_Number: item.data[1],
                        Area: item.data[2],
                        Operation_Status: item.data[3],
                        Current_Status: item.data[4],
                        Commands_List: item.data[5],
                        Recently_Abnormal_State: item.data[6],
                        Operations: item.data[7],
                        Area_Id: item.userdata.areaId,
                        IS_Exception: item.userdata.status,
                    }, headerAxios).then(function (res) {
                        if (res.data.status) {
                            resolve({
                                data: {
                                    status: true,
                                    msg: 'บันทึกข้อมูลสำเร็จ',
                                    data: null
                                }
                            })
                        } else {
                            resolve({
                                data: {
                                    status: false,
                                    msg: 'ไม่สามารถบันทึกข้อมูลได้',
                                    data: null
                                }
                            })
                        }
                    })
                }
            } else {
                const headerAxios = {
                    headers: {
                        'Content-Type': 'application/json',
                        'X-REST-METHOD': 'PUT'
                    }
                }
                axios.post('https://medhr.medicine.psu.ac.th/app-api/v2/?/apis/hrtime/monitor_device/', {
                    Device_Name: item.data[0],
                    Serial_Number: item.data[1],
                    Area: item.data[2],
                    Operation_Status: item.data[3],
                    Current_Status: item.data[4],
                    Commands_List: item.data[5],
                    Recently_Abnormal_State: item.data[6],
                    Operations: item.data[7],
                    Area_Id: item.userdata.areaId,
                    IS_Exception: item.userdata.status,
                }, headerAxios).then(function (res) {
                    if (res.data.status) {
                        resolve({
                            data: {
                                status: true,
                                msg: 'บันทึกข้อมูลสำเร็จ',
                                data: null
                            }
                        })
                    } else {
                        resolve({
                            data: {
                                status: false,
                                msg: 'ไม่สามารถบันทึกข้อมูลได้',
                                data: null
                            }
                        })
                    }
                })
            }
        })
    })
}

function SendLine(msg) {
    var param = querystring.stringify(
        {
            message: msg,
            // Token: 'eQDlEEnpTF1OoeNMU8hZ8ThOBVCafVqtxo2r1ZACbA2' // ส่วนตัว
            // Token: 'Y1mwz7oX0Ln0Ll0Ppdj5y8fE93mMezlb1yVW9Y7oWV1'
            Token: 'rce8CYRfG5EFbMuCGWdKCyJyB9XMbHglT0WZwWbu8pW' // เตือนลงกลุ่ม แจ้งเตือนอุปกรณ์ Access device ZK
        }
    )

    axios.post('https://medhr.medicine.psu.ac.th/sendLineNotify.php', param)
    // console.log('Line Notify Success')
}


function getDataFormZK() {
    return new Promise(function (resolutionFunc, rejectionFunc) {
        var header = {
            headers: {
                Cookie: Cookies,
                withCredentials: true
            }
        }
        try {
            axios.get('http://172.29.154.252:8088/accDevMonitorAction!getDevMonitorInfo.action?systemCode=acc', header).then(function (res) {
                if (res.status == 201) {
                    getCookies().then(function (res) {
                        if (res.data.status) {
                            Cookies = res.data.data
                            resolutionFunc({data: {status: true, data: null}})
                        } else {
                            rejectionFunc({data: {status: false, data: null}})
                        }
                    })
                } else {
                    resolutionFunc({data: {status: true, data: res.data.rows}})
                }
            })
        } catch (e) {
            console.log(e)
        } finally {

        }
    })
}

function getCookies() {
    return new Promise(function (resolutionFunc, rejectionFunc) {
        axios.post('http://172.29.154.252:8088/authLoginAction!login.do?username=chaiyan&password=chaiyan2020', '').then(function (res) {
            if (res.data.ret == 'ok') {
                resolutionFunc({
                    data: {
                        status: true,
                        data: res.headers['set-cookie'][0] + ':' + res.headers['set-cookie'][1] + ':' + res.headers['set-cookie'][2]
                    }
                })
            } else {
                rejectionFunc({data: {status: false, data: null}})
            }
        })
    })
}

